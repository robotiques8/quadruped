import argparse
import math
import pybullet as p
from time import sleep

dt = 0.01

def init():
    """Initialise le simulateur
    
    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot
    
    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)
    
    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation
    
    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints

if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, t = args.m, args.x, args.y, args.t
    
    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')
    else:
        raise Exception('Mode non implémenté: %s' % mode)
    
    t = 0

    # Boucle principale
    while True:
        t += dt

        if mode == 'demo':
            # Récupération des positions cibles
            joints = demo(t, p.readUserDebugParameter(amplitude))

        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)

        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)
